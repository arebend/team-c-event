require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');
const router = require('./routes');
const cors = require('cors');

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({
  extended: false
}));

if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'));

app.use('/api/v1', router);

module.exports = app;