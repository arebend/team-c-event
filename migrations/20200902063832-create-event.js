'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Events', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      location: {
        allowNull: false,
        type: Sequelize.STRING
      },
      image: {
        allowNull: false,
        type: Sequelize.STRING
      },
      dateTimeStart: {
        allowNull: false,
        type: Sequelize.DataTypes.DATE
      },
      dateTimeEnd: {
        allowNull: false,
        type: Sequelize.DataTypes.DATE
      },
      fee: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER
      },
      status: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING
      },
      categoryId: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Events');
  }
};