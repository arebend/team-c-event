const router = require('express').Router();
const routerEvent = require('./routerEvent');
const routerBooking = require('./routerBooking');

router.use('/event', routerEvent);
router.use('/booking', routerBooking);


module.exports = router;