const router = require('express').Router();
const booking = require('../controllers/bookingController');

router.post('/create', booking.createBooking);
// router.put('/update/:id', booking.updateBooking);
router.get('/detail', booking.detailBooking);
router.get('/all', booking.allBooking);


module.exports = router;