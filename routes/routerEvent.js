const router = require('express').Router();
const event = require('../controllers/eventController');
const uploader = require('../middlewares/uploader');
const authenticate = require('../middlewares/authentication');

router.post('/create', authenticate, uploader.single('image'), event.createEvent);
router.put('/update/:id', authenticate, uploader.single('image'), event.updateEvent);
router.get('/one/:id', event.detailEvent);
router.get('/all', event.showAllEvent);
router.get('/category/:id', event.eventByCategory);
router.get('/categories', event.showAllCategory);

module.exports = router;