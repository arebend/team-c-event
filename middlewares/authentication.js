require('dotenv').config();
const response = require('../helpers/responseFormatter');

module.exports = (req, res, next) => {
  const head = req.headers;
  if (!head.authorization) res.status(401).json(response.error(new Error('Unauthorized')));
  if (head.authorization === process.env.AUTH) return next();

  res.status(403).json(
    response.error(new Error('Invalid Authorizaion Key'))
  );
};