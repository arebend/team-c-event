const sgMail = require('@sendgrid/mail');

async function sendMail(nganu) {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY); // this function for setting sendgrid api
  const msg = {
    to: nganu.email,
    from: 'geikisen@gmail.com',
    subject: 'Event Booking Notification',
    html: `<html>
    <img src="https://ik.imagekit.io/0p0l0n0g0/rsz_1eventfinderco_HvIxQomKM.jpg" alt="EventFinder Logo">
         <h3>Event Finder Booking Notification</h3>
         <p>Hi ${nganu.email},</p>
         <p>This is your Booking Details:
            <ul>
                <li>Booking Id: ${nganu.booking_id}</li>
                <li>Event Title: ${nganu.Event.title}</li>
                <li>Location: ${nganu.Event.location}</li>
                <li>Start: ${nganu.Event.dateTimeStart}</li>
                <li>End: ${nganu.Event.dateTimeEnd}</li>
                <li>Fee: Rp${nganu.Event.fee}</li>
            </ul>
         </p>

         <h4>Enjoy The Event! </h4>
         </html>`

  };
  try {
    return await sgMail.send(msg);
  } catch (err) {
    console.log(err);
  }
}
module.exports = {
  sendMail
};