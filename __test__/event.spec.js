const request = require('supertest')
const app = require('../server')
const db = require('../models')
const { Event, Category } = require('../models')

describe('Events API Collection', () => {
    beforeAll(async () => {
        await Event.destroy({
            trucate: true
        })

        await Category.create({
            name: 'Food and Drinks',
            image: 'file:///home/asus/Coding/finalProject/back-end/lib/image_collection/hideThePain.jpeg'
        })
    })

    afterAll(() => {
        Category.destroy({
            where: {
                name: 'Food and Drinks'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('/api/v1/event/')
})