const {
  Event,
  Category
} = require('../models');
const response = require('../helpers/responseFormatter');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const imagekit = require('../lib/imagekit');

module.exports = {
  async createEvent(req, res) {
    let {
      title,
      description,
      location,
      dateTimeStart,
      dateTimeEnd,
      fee,
      status,
      categoryId
    } = req.body;
    const split = await req.file.originalname.split('.');
    const ext = await split[split.length - 1];
    const eventImage = await imagekit.upload({
      file: req.file.buffer,
      fileName: `IMG-${Date.now()}.${ext}`
    });
    try {
      let event = await Event.create({
        title,
        description,
        location,
        image: eventImage.url,
        dateTimeStart,
        dateTimeEnd,
        fee,
        status,
        categoryId
      });
      const createdEvent = await Event.findOne({
        where: {
          id: event.id
        },
        include: [
          { model: Category, as: 'categoryInfo' }
        ]
      });
      res.status(201).json(
        response.success({
          createdEvent
        })
      );
    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
      console.error(err);
    }
  },
  async updateEvent(req, res) {
    let {
      title,
      description,
      location,
      dateTimeStart,
      dateTimeEnd,
      fee,
      status,
      categoryId
    } = req.body;
    const file = req.file;
    if (file) {
      const split = await req.file.originalname.split('.');
      const ext = await split[split.length - 1];
      const eventImage = await imagekit.upload({
        file: req.file.buffer,
        fileName: `IMG-${Date.now()}.${ext}`
      });
      try {
        let dataUpdate = {
          title,
          description,
          location,
          image: eventImage.url,
          dateTimeStart,
          dateTimeEnd,
          fee,
          status,
          categoryId
        };

        await Event.update(dataUpdate, {
          where: {
            id: req.params.id
          }
        });
        const updatedEvent = await Event.findOne({
          where: {
            id: req.params.id
          },
          include: [
            { model: Category, as: 'categoryInfo' }
          ]
        });
        res.status(200).json(
          response.success({
            updatedEvent
          })
        );
      } catch (err) {
        res.status(422).json(
          response.error(err)
        );
      }
    } else {
      try {
        let dataUpdate = {
          title,
          description,
          location,
          dateTimeStart,
          dateTimeEnd,
          fee,
          status,
          categoryId
        };

        await Event.update(dataUpdate, {
          where: {
            id: req.params.id
          }
        });
        const updatedEvent = await Event.findOne({
          where: {
            id: req.params.id
          },
          include: [
            { model: Category, as: 'categoryInfo' }
          ]
        });
        res.status(200).json(
          response.success({
            updatedEvent
          })
        );
      } catch (err) {
        res.status(422).json(
          response.error(err)
        );
      }
    }
  },
  async detailEvent(req, res) {
    try {
      let event = await Event.findOne({
        where: {
          id: req.params.id
        },
        include: [{
          model: Category,
          as: 'categoryInfo',
          attributes: ['id', 'name', 'image']
        }]
      });
      res.status(200).json(
        response.success({
          event
        })
      );

    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
    }
  },

  async showAllEvent(req, res) {
    try {
      const feeOperationFilter = req.query.fee === 'free' ? Op.eq : Op.gt;
      const dateOperationFilter = req.query.date === 'past' ? Op.lt : Op.gt;

      const filterByCategory = req.query.category ? ({ categoryId: req.query.category }) : {};
      const filterByFee = req.query.fee ? ({ fee: { [feeOperationFilter]: 0 } }) : {};
      const filterByDate = req.query.date ? ({ dateTimeEnd: { [dateOperationFilter]: Date.now() } }) : {};

      let count = await Event.count({
        where: {
          [Op.and]: [
            filterByCategory,
            filterByFee,
            filterByDate
          ]
        },
      });
      let page = await Math.floor(count / 6) + 1;
      let events = await Event.findAll({
        where: {
          [Op.and]: [
            filterByCategory,
            filterByFee,
            filterByDate
          ]
        },
        include: [{
          model: Category,
          as: 'categoryInfo',
          attributes: ['id', 'name', 'image']
        }],
        limit: 6,
        offset: (req.query.page - 1) * 6,
        order: [
          ['id', 'ASC'],
        ]
      });
      res.status(200).json(
        response.success({
          count,
          page,
          events
        })
      );
    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
      console.error(err);
    }
  },
  async eventByCategory(req, res) {
    try {
      const feeOperationFilter = req.query.fee === 'free' ? Op.eq : Op.gt;
      const dateOperationFilter = req.query.date === 'past' ? Op.gt : Op.lt;
      let count = await Event.count({
        where: {
          [Op.or]: [
            {
              fee: {
                [feeOperationFilter]: 0
              }
            },
            {
              dateTimeEnd: {
                [dateOperationFilter]: Date.now()
              }
            }]
        },
        include: [{
          model: Category,
          as: 'categoryInfo',
          where: {
            id: req.params.id
          }
        }]
      });
      let page = await Math.floor(count / 6) + 1;
      let events = await Event.findAll({
        include: [{
          model: Category,
          as: 'categoryInfo',
          where: {
            id: req.params.id
          }
        }],
        limit: 6,
        offset: (req.query.page - 1) * 6,
        order: [
          ['id', 'ASC'],
        ]
      });
      res.status(200).json(
        response.success({
          count,
          page,
          events
        })
      );

    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
      console.error(err);
    }
  },
  async showAllCategory(req, res) {
    try {
      let categories = await Category.findAll({
        order: [
          ['id', 'ASC']
        ]
      });
      res.status(200).json(
        response.success({
          categories
        })
      );

    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
    }
  }
};

