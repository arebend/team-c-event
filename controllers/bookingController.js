const {
  Booking,
  Event
} = require('../models');
const response = require('../helpers/responseFormatter');
const makeid = require('../helpers/random');
const {
  sendMail
} = require('../helpers/bookingMail');

module.exports = {

  async createBooking(req, res) {
    try {
      const user = await Booking.findOne({
        where: {
          email: req.body.email,
          eventId: req.body.eventId
        }
      });
      if (user) {
        throw new Error('email has already use in this event');
      }

      const {
        email,
        status,
        eventId,
      } = req.body;

      const book = await Booking.create({
        email,
        status,
        eventId,
        booking_id: makeid()
      });
      const bookDetails = await Booking.findOne({
        where: {
          id: book.id
        },
        include: [{
          model: Event
        }]
      });
      sendMail(bookDetails);

      res.status(201).json(
        response.success({
          bookDetails
        })
      );
    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
    }
  },
  // async updateBooking(req, res) {
  //   try {
  //     const {
  //       email,
  //       eventId
  //     } = req.body;
  //     const dataUpdated = {
  //       email,
  //       eventId
  //     };
  //     await Booking.update(dataUpdated, {
  //       where: {
  //         id: req.params.id
  //       }
  //     });
  //     res.status(200).json(
  //       response.success({
  //         dataUpdated
  //       })
  //     );
  //   } catch (err) {
  //     res.status(422).json(
  //       response.error(err)
  //     );
  //   }
  // },

  async detailBooking(req, res) {
    try {
      let booking = await Booking.findOne({
        where: {
          id: req.body.id
        }
      });
      res.status(200).json(
        response.success({
          booking
        })
      );
    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
    }
  },

  async allBooking(req, res) {
    try {
      let allBook = await Booking.findAll({
        where: {
          email: req.body.email
        }
      });
      res.status(200).json(
        response.success({
          allBook
        })
      );
    } catch (err) {
      res.status(422).json(
        response.error(err)
      );
    }
  }

};