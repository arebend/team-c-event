module.exports = {
  testEnvironment: 'node', 
  verbose: true ,
  moduleNameMapper: {
        
    '@sendgrid/mail':  '<rootDir>/__mocks__/@sendgrid/mail'
  }
};