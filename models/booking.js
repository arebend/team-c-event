'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Booking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Booking.belongsTo(models.Event, {
        foreignKey: 'eventId'
      });
    }
  }
  Booking.init({
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: {
          args: true,
          msg: 'Incorrect email format or email should be not empty'
        }
      },
      allowNull: {
        args: false,
        msg: 'Email must be filled'
      }
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'active'
    },
    eventId: DataTypes.INTEGER,
    booking_id: {
      type: DataTypes.STRING,
    }
  }, {
    sequelize,
    modelName: 'Booking',
  });
  return Booking;
};