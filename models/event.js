'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Event extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Event.belongsTo(models.Category,{
        foreignKey: 'categoryId',
        as: 'categoryInfo'
      });
      Event.hasMany(models.Booking, {
        foreignKey: 'eventId'
      });
    }
  }
  Event.init({
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Title must be filled'
        }
      },
      allowNull: {
        args: true,
        msg: 'Title must be filled'
      }
    },
    description: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Description must be filled'
        }
      },
      allowNull: {
        args: false,
        msg: 'Description must be filled'
      }
    },
    location: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Location must be filled'
        }
      },
      allowNull: {
        args: false,
        msg: 'Location must be filled'
      }
    },
    image: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Image must be filled'
        }
      },
      allowNull: {
        args: false,
        msg: 'Image must be filled'
      }
    },
    dateTimeStart: {
      type: DataTypes.DATE,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Date time start must be filled'
        }
      },
      allowNull: {
        args: false,
        msg: 'Date time start must be filled'
      }
    },
    dateTimeEnd: {
      type: DataTypes.DATE,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Date time end must be filled'
        }
      },
      allowNull: {
        args: false,
        msg: 'Date time end must be filled'
      }
    },
    fee: DataTypes.INTEGER,
    status: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Status must be filled'
        }
      },
      allowNull: {
        args: false,
        msg: 'Status must be filled'
      }
    },
    categoryId: {
      type: DataTypes.INTEGER,
      allowNull: {
        args: false,
        msg: 'categoryId must be filled'
      },
      validate: {
        min: 1,
        max: 6
      }
    }
  }, {
    sequelize,
    modelName: 'Event',
  });
  return Event;
};