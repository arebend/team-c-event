'use strict';

module.exports = {
  up: async (queryInterface) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Categories', [{
      name: 'Food & Drinks',
      image: 'https://ik.imagekit.io/0p0l0n0g0/foodanddrinks_g5kLeFA1e.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Hobbies',
      image: 'https://ik.imagekit.io/0p0l0n0g0/hobbies__1__S8ek4RD-HX4.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Sport & Fitness',
      image: 'https://ik.imagekit.io/0p0l0n0g0/fitness_urQQAkaTmUU.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Music',
      image: 'https://ik.imagekit.io/0p0l0n0g0/music_64KtYy61B.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Science & Tech',
      image: 'https://ik.imagekit.io/0p0l0n0g0/scientis-min_InhBSPEpKS5.jpeg',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Others',
      image: 'https://ik.imagekit.io/0p0l0n0g0/libary_q6TC2lv2-PX.jpeg',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ], {});
  },

  down: async (queryInterface) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Categories', null,{});
  }
};
